﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nave : MonoBehaviour
{
    public float velocidadX;
    public float maxX;
    private float posX;
    private float direction;

    // Update is called once per frame
    void Update()
    {

        direction = Input.GetAxis("Horizontal");

        Debug.Log(transform.position.x);
        Debug.Log(transform.rotation.x);

        posX = transform.position.x + direction * velocidadX * Time.deltaTime;

        if (posX > maxX)
        {
            posX = maxX;
        }
        else if (posX < -maxX)
        {
            posX = -maxX;
        }

        transform.position = new Vector3(posX,transform.position.y, transform.position.z);
    }
}