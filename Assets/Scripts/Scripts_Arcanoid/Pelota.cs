﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pelota : MonoBehaviour
{
    private Vector2 ballPosition;
    public Vector2 ballVelocity;

    public UnityEngine.UI.Text vidas;
    public int nvidas = 3;

    void Awake()
    {
        ballPosition = transform.position;
        vidas.text = nvidas.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        ballPosition.y = transform.position.y + ballVelocity.y * Time.deltaTime;
        ballPosition.x = transform.position.x + ballVelocity.x * Time.deltaTime;

        transform.position = new Vector3(ballPosition.x, ballPosition.y, transform.position.z);
    }

    /// <summary>
    /// Sent when an incoming collider makes contact with this object's
    /// collider (2D physics only).
    /// </summary>
    /// <param name="other">The Collision2D data associated with this collision.</param>
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Muro")
        {
            ballVelocity.y *= -1f;
        }

        else if (other.gameObject.tag == "Nave")
        {
            ballVelocity.y *= -1f;
        }

        else if (other.gameObject.tag == "Ladrillo")
        {
            ballVelocity.x *= 1f;
            ballVelocity.y *= -1f;
            Destroy (other.gameObject);
        }

        else if (other.gameObject.tag == "Muro2")
        {
            ballVelocity.x *= -1f;
        }

        else if (other.gameObject.tag == "Muerte")
        {
            transform.position = new Vector3(0,-2.39f,0);
            ballVelocity.y *=-1f;          
            nvidas--;
            vidas.text = nvidas.ToString();
            if (nvidas == 0)
            {
                Destroy (this.gameObject);
            }
        }
    }

}